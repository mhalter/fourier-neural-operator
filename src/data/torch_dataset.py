import torch
import logging
import enum
from typing import Tuple
import itertools
from utils.filesystem_helper import get_files
import numpy as np

from utils.math import linear_interpolation


class Encoding(enum.Enum):
    PLAIN = 'PLAIN'
    DIST_ENC = 'DIST_ENC'
    MASK_ENC = 'MASK_ENC'
    SQUARE_ENC = 'SQUARE_ENC'
    MIXED_ENC = 'MIXED_ENC'

    @classmethod
    def _missing_(cls, value):
        return cls.PLAIN


def get_num_features(encoding: Encoding) -> int:
    if encoding == Encoding.PLAIN:
        return 8
    elif encoding in [Encoding.DIST_ENC, Encoding.MASK_ENC, Encoding.SQUARE_ENC]:
        return 9
    elif encoding == Encoding.MIXED_ENC:
        return 11
    else:
        raise NotImplementedError


class TorchDataset(torch.utils.data.Dataset):
    def __init__(self, path: str, dtype: torch.dtype = torch.float32, sample_rate: int = 10, encoding: str = 'PLAIN'):
        self.sample_rate = sample_rate
        self.dtype = dtype
        self.encoding = Encoding(encoding)
        self.logger = logging.getLogger('TorchDataset')

        self.logger.debug('loading dataset from \'{}\''.format(path))
        self.cases = get_files(path, file_extension='npz', key_patterns={
            'N': r'N(\d{1,2})',
            'case': r'(C\d{4})'
        }, sort_pattern=r'C(\d{2})')
        self.N = sum([int(i['N']) for i in self.cases])
        self.case_index_list = list(itertools.chain.from_iterable(
            [zip(int(item['N']) * [idx], range(int(item['N']))) for idx, item in enumerate(self.cases)]
        ))

        npz = np.load(self.cases[0]['path'])
        grid = npz['grid'][0, ...]
        t = npz['t'][0, ...].astype(float)
        self.D_x = grid.shape[0]
        self.D_y = grid.shape[1]
        self.D_z = grid.shape[1]
        self.D_t = t.shape[0]
        self.in_features = get_num_features(self.encoding)
        self.D_t_out = (self.D_t // self.sample_rate) * self.sample_rate
        self.output_size = (self.D_x, self.D_y, self.D_z, self.D_t_out, 4)

        self.logger.info('Dataset')
        self.logger.info(f'encoding: {encoding}')
        self.logger.info(f'samples:  {self.N}')
        self.logger.info(f'shape:    ({self.D_x}, {self.D_y}, {self.D_z}, {self.D_t}, {self.in_features})')

    def __len__(self):
        return self.N

    def __getitem__(self, idx) -> Tuple[torch.Tensor, torch.Tensor, int]:
        case_idx, item_idx = self.case_index_list[idx]
        npz = np.load(self.cases[case_idx]['path'])
        grid = npz['grid'][item_idx, ...]
        t = npz['t'][item_idx, ...].astype(float)
        coords = np.concatenate([
            np.tile(grid.reshape((self.D_x, self.D_y, self.D_z) + (1, 3)), (1, 1, 1, self.D_t, 1)),
            np.tile(t.reshape(3 * (1,) + (self.D_t, 1)), (self.D_x, self.D_y, self.D_z) + (1, 1))
        ], axis=-1)
        data = npz['data'][item_idx, ...]
        if self.encoding == Encoding.DIST_ENC:
            bnd_enc = np.tile(npz['boundary_encoding'][item_idx, ..., np.newaxis, 1:2],
                              (1, 1, 1, self.D_t, 1))[..., :self.D_t_out, :]
        elif self.encoding == Encoding.MASK_ENC:
            bnd_enc = np.tile(npz['boundary_encoding'][item_idx, ..., np.newaxis, 0:1],
                              (1, 1, 1, self.D_t, 1))[..., :self.D_t_out, :]
        elif self.encoding == Encoding.SQUARE_ENC:
            bnd_enc = np.tile(npz['boundary_encoding'][item_idx, ..., np.newaxis, 2:3],
                              (1, 1, 1, self.D_t, 1))[..., :self.D_t_out, :]
        elif self.encoding == Encoding.MIXED_ENC:
            bnd_enc = np.tile(npz['boundary_encoding'][item_idx, ..., np.newaxis, :],
                              (1, 1, 1, self.D_t, 1))[..., :self.D_t_out, :]
        else:
            bnd_enc = np.empty((self.D_x, self.D_y, self.D_z, self.D_t_out, 0))
        samples_a = np.tile(np.arange(0, data.shape[3] - self.sample_rate, self.sample_rate).reshape(-1, 1),
                            (1, self.sample_rate)).flatten()
        samples_b = np.tile(np.arange(self.sample_rate, data.shape[3], self.sample_rate).reshape(-1, 1),
                            (1, self.sample_rate)).flatten()
        n = samples_a.shape[0]
        idx_list = np.arange(samples_a.shape[0])
        lin_int = linear_interpolation(coords[..., idx_list, 3], coords[..., samples_a, 3], coords[..., samples_b, 3],
                                       data[..., samples_a, :4], data[..., samples_b, :4])
        x = torch.tensor(np.concatenate([coords[..., :n, :], bnd_enc[..., :n, :], lin_int], axis=-1),
                         dtype=self.dtype)
        y = torch.tensor(data[..., :n, :4], dtype=self.dtype)
        return x, y, idx


if __name__ == '__main__':
    from utils.logger import set_config

    PATH = '/home/moritz/Documents/fourier_neural_operator/data/numpy/pipe_selection_small'

    set_config('DEBUG', '/home/moritz/Documents/fourier_neural_operator/log/debug.log')
    ds = TorchDataset(PATH)
