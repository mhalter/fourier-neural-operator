from typing import Tuple, List, Dict
import numpy as np
from vtk import (
    vtkPoints,
    vtkUnstructuredGridReader,
    vtkResampleWithDataSet,
    vtkTransformFilter,
    vtkTransform,
    vtkAlgorithmOutput,
    vtkGeometryFilter,
    vtkImplicitPolyDataDistance
)
from vtk.util.numpy_support import vtk_to_numpy

from utils.csv_helper import CsvRow
from utils.filesystem_helper import get_vtk_file_series
from utils.vtk_helper import (
    get_array,
    get_transform,
    get_normal_uniform_grid
)


def combine(key: str, samples: List[Dict[str, np.ndarray]]) -> np.ndarray:
    res = []
    for s in samples:
        res.append(s[key])
    return np.stack(res, axis=0)


class VtkSample:
    def __init__(self, N: int):
        self.N = N

        self.reader = vtkUnstructuredGridReader()
        self.resampler = vtkResampleWithDataSet()
        self.transform_filter = vtkTransformFilter()
        self.transform_filter.SetInputData(get_normal_uniform_grid(self.N))
        self.transform_filter.SetTransform(vtkTransform())

        self.resampler.SetSourceConnection(self.reader.GetOutputPort())

        self.geometry_filter = vtkGeometryFilter()
        self.distance_function = vtkImplicitPolyDataDistance()

    def process(self, path: str, rows: List[CsvRow]) -> Tuple[np.ndarray, np.ndarray, np.ndarray, np.ndarray]:
        samples = []
        for row in rows:
            vtk_files = get_vtk_file_series(path)
            self.transform_filter.SetTransform(get_transform(row))
            self.transform_filter.Update()
            self.resampler.SetInputData(self.transform_filter.GetOutput())
            t = []
            boundary_encoding = None
            data = []
            for file in vtk_files:
                shape = 3 * (self.N,)
                self.reader.SetFileName(file[0])
                self.resampler.Update()
                output = self.resampler.GetOutput()
                point_data = output.GetPointData()
                U = get_array('U', point_data).reshape(shape + (3,))
                p = get_array('p', point_data).reshape(shape + (1,))
                if boundary_encoding is None:
                    points = output.GetPoints()
                    grid = vtk_to_numpy(points.GetData()).astype('float32').reshape(shape + (3,))
                    boundary_encoding = self.get_boundary_encoding(points, point_data).reshape(shape + (3,))
                wss = get_array('wallShearStress', point_data).reshape(shape + (3,))
                t.append(file[1])
                data.append(np.concatenate([U, p, wss], axis=-1))
            samples.append({
                'grid': grid,
                't': np.array(t),
                'data': np.stack(data, axis=3),
                'boundary_encoding': boundary_encoding
            })

        return (combine('grid', samples), combine('t', samples), combine('data', samples),
                combine('boundary_encoding', samples))

    def set_transformation(self, row: CsvRow) -> None:
        self.transform_filter.SetTransform(get_transform(row))
        self.transform_filter.Update()
        self.resampler.SetInputData(self.transform_filter.GetOutput())
        self.geometry_filter.SetInputData(self.reader.GetOutput())
        self.resampler.Update()

    def load_preview(self, file: str) -> None:
        self.reader.SetFileName(file)
        self.reader.Update()
        self.geometry_filter.SetInputData(self.reader.GetOutput())
        self.geometry_filter.Update()

    def get_source(self, name: str) -> vtkAlgorithmOutput:
        if name == 'READER':
            return self.reader.GetOutputPort()
        elif name == 'RESAMPLER':
            self.resampler.Update()
            out = self.resampler.GetOutput()
            out.GetPointData().RemoveArray('vtkGhostType')
            out.GetCellData().RemoveArray('vtkGhostType')
            out.GetPointData().SetScalars(out.GetPointData().GetArray('p'))
            return out
        raise KeyError

    def get_boundary_encoding(self, points: vtkPoints, point_data) -> np.ndarray:
        self.geometry_filter.SetInputData(self.reader.GetOutput())
        self.geometry_filter.Update()
        self.distance_function.SetInput(self.geometry_filter.GetOutput())
        result = []
        mag_U = np.sqrt(np.sum(np.square(get_array('U', point_data)), axis=-1))
        for idx in range(points.GetNumberOfPoints()):
            dist = abs(self.distance_function.EvaluateFunction(points.GetPoint(idx)))
            result.append(np.array([0.0 if mag_U[idx] == 0.0 else 1.0,
                                    dist * (-1.0 if mag_U[idx] == 0.0 else 1.0),
                                    dist if mag_U[idx] == 0.0 else 0.0]))
        result = np.stack(result, axis=0)
        max_dist = np.max(result[..., 2])
        max_dist_squared = np.square(max_dist)
        result[..., 2] = np.where(result[..., 0] == 1.0, np.square(max_dist_squared - np.square(max_dist - result[..., 2])), 0.0)
        return result


class VtkCase:
    def __init__(self, N: int):
        self.N = N
        self.reader = vtkUnstructuredGridReader()
        self.resampler_list: List[vtkResampleWithDataSet] = []

    def load_case(self, path: str, rows: List[CsvRow]):
        self.reader.SetFileName(path)
        self.reader.Update()
        for row in rows:
            transform_filter = vtkTransformFilter()
            transform_filter.SetInputData(get_normal_uniform_grid(self.N))
            transform_filter.SetTransform(get_transform(row))
            transform_filter.Update()

            resampler = vtkResampleWithDataSet()
            resampler.SetSourceConnection(self.reader.GetOutputPort())
            resampler.SetInputData(transform_filter.GetOutput())
            resampler.Update()
            self.resampler_list.append(resampler)

    def get_reader_output(self) -> vtkAlgorithmOutput:
        return self.reader.GetOutputPort()

    def get_resampler_outputs(self) -> List[vtkAlgorithmOutput]:
        result = []
        for resampler in self.resampler_list:
            out = resampler.GetOutput()
            out.GetPointData().RemoveArray('vtkGhostType')
            out.GetCellData().RemoveArray('vtkGhostType')
            out.GetPointData().SetScalars(out.GetPointData().GetArray('p'))
            result.append(out)
        return result
