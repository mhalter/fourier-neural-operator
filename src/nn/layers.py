import torch
import torch.nn as nn
from torch.fft import rfftn, irfftn
import torch.nn.functional as functional

import operator
from functools import reduce
from logging import getLogger
from typing import Tuple

from utils.math import complex_mul4d

logger = getLogger(__name__)


class SpectralConv4dFast(nn.Module):
    def __init__(self, in_channels: int, out_channels: int, n_modes_1: int,
                 n_modes_2: int, n_modes_3: int, n_modes_4: int):
        super(SpectralConv4dFast, self).__init__()

        self.in_channels = in_channels
        self.out_channels = out_channels
        self.n_modes_1 = n_modes_1
        self.n_modes_2 = n_modes_2
        self.n_modes_3 = n_modes_3
        self.n_modes_4 = n_modes_4

        self.scale = 1 / (in_channels * out_channels)

        self.weights_1 = nn.Parameter(self.scale * torch.rand(in_channels, out_channels, n_modes_1,
                                                              n_modes_2, n_modes_3, n_modes_4, 2))
        self.weights_2 = nn.Parameter(self.scale * torch.rand(in_channels, out_channels, n_modes_1,
                                                              n_modes_2, n_modes_3, n_modes_4, 2))
        self.weights_3 = nn.Parameter(self.scale * torch.rand(in_channels, out_channels, n_modes_1,
                                                              n_modes_2, n_modes_3, n_modes_4, 2))
        self.weights_4 = nn.Parameter(self.scale * torch.rand(in_channels, out_channels, n_modes_1,
                                                              n_modes_2, n_modes_3, n_modes_4, 2))

        self.weights_5 = nn.Parameter(self.scale * torch.rand(in_channels, out_channels, n_modes_1,
                                                              n_modes_2, n_modes_3, n_modes_4, 2))
        self.weights_6 = nn.Parameter(self.scale * torch.rand(in_channels, out_channels, n_modes_1,
                                                              n_modes_2, n_modes_3, n_modes_4, 2))
        self.weights_7 = nn.Parameter(self.scale * torch.rand(in_channels, out_channels, n_modes_1,
                                                              n_modes_2, n_modes_3, n_modes_4, 2))
        self.weights_8 = nn.Parameter(self.scale * torch.rand(in_channels, out_channels, n_modes_1,
                                                              n_modes_2, n_modes_3, n_modes_4, 2))

    def forward(self, x: torch.Tensor) -> torch.Tensor:
        batch_size = x.size(0)

        x_ft = rfftn(x, s=(x.size(-4), x.size(-3), x.size(-2), x.size(-1)), norm='forward')

        out_ft = torch.zeros(batch_size, self.in_channels, x.size(-4), x.size(-3), x.size(-2), x.size(-1) // 2 + 1,
                             device=x.device,
                             dtype=torch.cfloat)

        out_ft[:, :, :self.n_modes_1, :self.n_modes_2, :self.n_modes_3, :self.n_modes_4] = \
            complex_mul4d(x_ft[:, :, :self.n_modes_1, :self.n_modes_2, :self.n_modes_3, :self.n_modes_4],
                          self.weights_1)
        out_ft[:, :, -self.n_modes_1:, :self.n_modes_2, :self.n_modes_3, :self.n_modes_4] = \
            complex_mul4d(x_ft[:, :, -self.n_modes_1:, :self.n_modes_2, :self.n_modes_3, :self.n_modes_4],
                          self.weights_2)
        out_ft[:, :, :self.n_modes_1, -self.n_modes_2:, :self.n_modes_3, :self.n_modes_4] = \
            complex_mul4d(x_ft[:, :, :self.n_modes_1, -self.n_modes_2:, :self.n_modes_3, :self.n_modes_4],
                          self.weights_3)
        out_ft[:, :, -self.n_modes_1:, -self.n_modes_2:, :self.n_modes_3, :self.n_modes_4] = \
            complex_mul4d(x_ft[:, :, -self.n_modes_1:, -self.n_modes_2:, :self.n_modes_3, :self.n_modes_4],
                          self.weights_4)
        out_ft[:, :, :self.n_modes_1, :self.n_modes_2, -self.n_modes_3:, :self.n_modes_4] = \
            complex_mul4d(x_ft[:, :, :self.n_modes_1, :self.n_modes_2, -self.n_modes_3:, :self.n_modes_4],
                          self.weights_5)
        out_ft[:, :, -self.n_modes_1:, :self.n_modes_2, -self.n_modes_3:, :self.n_modes_4] = \
            complex_mul4d(x_ft[:, :, -self.n_modes_1:, :self.n_modes_2, -self.n_modes_3:, :self.n_modes_4],
                          self.weights_6)
        out_ft[:, :, :self.n_modes_1, -self.n_modes_2:, -self.n_modes_3:, :self.n_modes_4] = \
            complex_mul4d(x_ft[:, :, :self.n_modes_1, -self.n_modes_2:, -self.n_modes_3:, :self.n_modes_4],
                          self.weights_7)
        out_ft[:, :, -self.n_modes_1:, -self.n_modes_2:, -self.n_modes_3:, :self.n_modes_4] = \
            complex_mul4d(x_ft[:, :, -self.n_modes_1:, -self.n_modes_2:, -self.n_modes_3:, :self.n_modes_4],
                          self.weights_8)

        x = irfftn(out_ft, (x.size(-4), x.size(-3), x.size(-2), x.size(-1)), norm='forward')
        return x


class SimpleBlock3d(nn.Module):
    def __init__(self, n_modes_1: int, n_modes_2: int, n_modes_3: int, n_modes_4: int,
                 width: int, out_dim: int, in_features: int):
        super(SimpleBlock3d, self).__init__()

        self.n_modes_1 = n_modes_1
        self.n_modes_2 = n_modes_2
        self.n_modes_3 = n_modes_3
        self.n_modes_4 = n_modes_4
        self.width = width
        self.fc_0 = nn.Linear(in_features, width)
        self.conv_0 = SpectralConv4dFast(self.width, self.width, self.n_modes_1,
                                         self.n_modes_2, self.n_modes_3, self.n_modes_4)
        self.conv_1 = SpectralConv4dFast(self.width, self.width, self.n_modes_1,
                                         self.n_modes_2, self.n_modes_3, self.n_modes_4)
        self.conv_2 = SpectralConv4dFast(self.width, self.width, self.n_modes_1,
                                         self.n_modes_2, self.n_modes_3, self.n_modes_4)
        self.conv_3 = SpectralConv4dFast(self.width, self.width, self.n_modes_1,
                                         self.n_modes_2, self.n_modes_3, self.n_modes_4)
        self.w_0 = nn.Conv1d(self.width, self.width, 1)
        self.w_1 = nn.Conv1d(self.width, self.width, 1)
        self.w_2 = nn.Conv1d(self.width, self.width, 1)
        self.w_3 = nn.Conv1d(self.width, self.width, 1)
        self.bn_0 = nn.BatchNorm1d(self.width)
        self.bn_1 = nn.BatchNorm1d(self.width)
        self.bn_2 = nn.BatchNorm1d(self.width)
        self.bn_3 = nn.BatchNorm1d(self.width)

        self.fc_1 = nn.Linear(self.width, 128)
        self.fc_2 = nn.Linear(128, out_dim)

    def forward(self, x: torch.Tensor) -> torch.Tensor:
        batch_size = x.size(0)
        size_x, size_y, size_z, size_t = x.shape[1], x.shape[2], x.shape[3], x.shape[4]
        x = self.fc_0(x)
        x = x.permute(0, 5, 1, 2, 3, 4)

        x_1 = self.conv_0(x)
        x_2 = self.w_0(x.view(batch_size, self.width, -1)).view(batch_size, self.width, size_x, size_y, size_z, size_t)
        x = self.bn_0((x_1 + x_2).view(batch_size, self.width, -1)).view(batch_size, self.width, size_x,
                                                                         size_y, size_z, size_t)
        x = functional.relu(x)

        x_1 = self.conv_1(x)
        x_2 = self.w_1(x.view(batch_size, self.width, -1)).view(batch_size, self.width, size_x, size_y, size_z, size_t)
        x = self.bn_1((x_1 + x_2).view(batch_size, self.width, -1)).view(batch_size, self.width, size_x,
                                                                         size_y, size_z, size_t)
        x = functional.relu(x)

        x_1 = self.conv_2(x)
        x_2 = self.w_2(x.view(batch_size, self.width, -1)).view(batch_size, self.width, size_x, size_y, size_z, size_t)
        x = self.bn_2((x_1 + x_2).view(batch_size, self.width, -1)).view(batch_size, self.width, size_x,
                                                                         size_y, size_z, size_t)
        x = functional.relu(x)

        x_1 = self.conv_3(x)
        x_2 = self.w_3(x.view(batch_size, self.width, -1)).view(batch_size, self.width, size_x, size_y, size_z, size_t)
        x = self.bn_3((x_1 + x_2).view(batch_size, self.width, -1)).view(batch_size, self.width, size_x,
                                                                         size_y, size_z, size_t)
        x = functional.relu(x)

        x = x.permute(0, 2, 3, 4, 5, 1)
        x = self.fc_1(x)
        x = functional.relu(x)
        x = self.fc_2(x)

        return x


class Net3d(nn.Module):
    def __init__(self, n_modes: int, width: int, out_dim: int, in_features: int):
        super(Net3d, self).__init__()
        self.conv = SimpleBlock3d(n_modes, n_modes, n_modes, n_modes // 2, width, out_dim, in_features)

    def forward(self, x: torch.Tensor) -> torch.Tensor:
        x = self.conv(x)
        return x.squeeze()

    def count_params(self) -> int:
        c = 0
        for p in self.parameters():
            c += reduce(operator.mul, list(p.size()))

        return c

    def save(self, opt_dict, epoch, loss, path: str) -> None:
        f_name = '{}.model'.format(path)
        logger.debug('saving model at \'{}\''.format(f_name))
        torch.save({
            'epoch': epoch,
            'model_state_dict': self.state_dict(),
            'optimizer_state_dict': opt_dict,
            'loss': loss
        }, f_name)

    def load(self, path: str, map_location) -> Tuple[dict, int]:
        logger.debug('load model from \'{}\''.format(path))
        checkpoint = torch.load(path, map_location=map_location)
        self.load_state_dict(checkpoint['model_state_dict'])
        return checkpoint['optimizer_state_dict'], checkpoint['epoch']
