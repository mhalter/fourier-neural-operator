from matplotlib.pyplot import Figure
from numpy import ndarray, where, concatenate


class ScatterPlot:
    def __init__(self, fig: Figure, pos: int, color_bar: bool = False, strip_mode: int = 0,
                 title: str = '', x_label: str = 'x', y_label: str = 'y', z_label: str = 'z'):
        self.fig = fig
        self.axes = fig.add_subplot(pos, projection='3d')
        self.axes.set_title(title)
        self.axes.set_xlabel(x_label)
        self.axes.set_ylabel(y_label)
        self.axes.set_zlabel(z_label)
        self.color_bar = color_bar
        self.strip_mode = strip_mode
        self.pos = None

    def scatter(self, grid: ndarray, data: ndarray):
        if self.pos is not None:
            self.pos.remove()
        if self.strip_mode == 2:
            x_grid = grid[[0, -1], ...].reshape([-1, 3])
            x_data = data[[0, -1], ...].reshape([-1])
            y_grid = grid[:, [0, -1], ...].reshape([-1, 3])
            y_data = data[:, [0, -1], ...].reshape([-1])
            z_grid = grid[:, :, [0, -1], ...].reshape([-1, 3])
            z_data = data[:, :, [0, -1], ...].reshape([-1])
            grid = concatenate([x_grid, y_grid, z_grid], axis=0)
            data = concatenate([x_data, y_data, z_data])
        else:
            if len(grid.shape) > 2:
                grid = grid.reshape([-1, 3])
            if len(data.shape) > 1:
                data = data.reshape([-1])
            if self.strip_mode == 1:
                non_zero = where(data != 0.0)
                grid = grid[non_zero]
                data = data[non_zero]
        self.pos = self.axes.scatter(grid[..., 0], grid[..., 1], grid[..., 2], c=data)
        if self.color_bar:
            self.fig.colorbar(self.pos, ax=self.axes)

    def set_view(self, elev: float, azim: float, roll: float) -> None:
        self.axes.view_init(elev=elev, azim=azim, roll=roll)
