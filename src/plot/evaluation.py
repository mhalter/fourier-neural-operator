import numpy as np
from os.path import join
from typing import Tuple
from scipy.io import loadmat
import matplotlib.pyplot as plt
from plot.scatter import ScatterPlot
from plot.slice import SlicePlotter
from time import sleep

ELEV = 30
AZIM = -75
ROLL = 0


class EvaluationPlotter:
    def __init__(self, path: str, sample_idx: int, time_idx: int, save_path: str = None):
        self.save_path = save_path
        self.sample_idx = sample_idx
        self.time_idx = time_idx
        self.elev = ELEV
        self.azim = AZIM
        self.roll = ROLL
        mat = loadmat(path)
        self.pred = mat['pred']
        self.truth = mat['truth']

    def plot_sample(self, feature_idx: int):
        feature_key = 'u'
        sup_title = 'u [m/s]'
        if feature_idx == 0:
            feature_key = 'u'
            sup_title = 'u [m/s]'
        elif feature_idx == 1:
            feature_key = 'v'
            sup_title = 'v [m/s]'
        elif feature_idx == 2:
            feature_key = 'w'
            sup_title = 'w [m/s]'
        elif feature_idx == 3:
            feature_key = 'p'
            sup_title = 'p [kPa]'

        plt.rcParams.update({'font.size': 22})
        fig = plt.figure(figsize=(50, 10))
        n, d_x, d_y, d_z, d_t, _ = self.pred.shape
        grid_x = np.tile(np.linspace(0, 1, d_x).reshape((d_x, 1, 1)), [1, d_y, d_z])
        grid_y = np.tile(np.linspace(0, 1, d_y).reshape((1, d_y, 1)), [d_x, 1, d_z])
        grid_z = np.tile(np.linspace(0, 1, d_z).reshape((1, 1, d_z)), [d_x, d_y, 1])
        truth = self.truth[self.sample_idx, ..., self.time_idx, feature_idx]
        pred = self.pred[self.sample_idx, ..., self.time_idx, feature_idx]
        selection = np.where(truth != 0.0)
        grid_x_selection = grid_x[selection[0], selection[1], selection[2]]
        grid_y_selection = grid_y[selection[0], selection[1], selection[2]]
        grid_z_selection = grid_z[selection[0], selection[1], selection[2]]
        truth_selection = truth[selection[0], selection[1], selection[2]]
        pred_selection = pred[selection[0], selection[1], selection[2]]
        ax = fig.add_subplot(131, projection='3d')
        sc = ax.scatter(grid_x_selection, grid_y_selection, grid_z_selection, c=truth_selection)
        ax.set_xlabel('x')
        ax.set_ylabel('y')
        ax.set_zlabel('z')
        ax.set_xticks([])
        ax.set_yticks([])
        ax.set_zticks([])
        ax.set_title('truth')
        fig.colorbar(sc, ax=ax)
        fig.suptitle(sup_title)

        ax = fig.add_subplot(132, projection='3d')
        sc = ax.scatter(grid_x_selection, grid_y_selection, grid_z_selection, c=pred_selection)
        ax.set_xlabel('x')
        ax.set_ylabel('y')
        ax.set_zlabel('z')
        ax.set_xticks([])
        ax.set_yticks([])
        ax.set_zticks([])
        ax.set_title('prediction')
        fig.colorbar(sc, ax=ax)

        ax = fig.add_subplot(133, projection='3d')
        sc = ax.scatter(grid_x_selection, grid_y_selection, grid_z_selection, c=np.abs(pred_selection - truth_selection))
        ax.set_xlabel('x')
        ax.set_ylabel('y')
        ax.set_zlabel('z')
        ax.set_xticks([])
        ax.set_yticks([])
        ax.set_zticks([])
        ax.set_title('absolute error')
        fig.colorbar(sc, ax=ax)

        if self.save_path is not None:
            plt.savefig(join(self.save_path, f'evaluation_{feature_key}'))
            plt.close()
        else:
            plt.show()

class EvaluationPlot:
    def __init__(self, path: str):
        mat = loadmat(path)
        self.pred: np.ndarray = mat['pred']
        self.truth: np.ndarray = mat['truth']

    def scatter(self, idx: int, v_idx: int, t_idx: int, file_path: str = None):
        p = self.pred[idx, ..., t_idx, v_idx]
        t = self.truth[idx, ..., t_idx, v_idx]

        fig = plt.figure()
        fig.suptitle(f'sample {idx} - t {t_idx}')
        pred = ScatterPlot(fig, 121, strip_mode=0, title='prediction')
        truth = ScatterPlot(fig, 122, strip_mode=0, title='truth')

        grid = np.stack([
            np.tile(np.linspace(0, 1, p.shape[0]).reshape((-1, 1, 1)), (1, p.shape[1], p.shape[2])),
            np.tile(np.linspace(0, 1, p.shape[1]).reshape((1, -1, 1)), (p.shape[0], 1, p.shape[2])),
            np.tile(np.linspace(0, 1, p.shape[2]).reshape((1, 1, -1)), (p.shape[0], p.shape[1], 1)),
        ], axis=-1)

        pred.scatter(grid, p)
        truth.scatter(grid, t)
        pred.set_view(ELEV, AZIM, ROLL)
        truth.set_view(ELEV, AZIM, ROLL)
        if file_path is None:
            plt.show()
        else:
            plt.savefig(file_path)
            plt.close()

    def cuts(self, idx: int, v_idx: int, t_idx: int, file_path: str = None):
        p = self.pred[idx, ..., t_idx, v_idx]
        t = self.truth[idx, ..., t_idx, v_idx]

        fig = plt.figure()
        fig.suptitle(f'sample {idx} - t {t_idx}')
        pred = SlicePlotter(fig, 231, x_idx=16, y_idx=16, z_idx=16, title='prediction')
        truth = SlicePlotter(fig, 234, x_idx=16, y_idx=16, z_idx=16, title='truth')

        pred.imshow(p)
        truth.imshow(t)
        if file_path is None:
            plt.show()
        else:
            plt.savefig(file_path)
            plt.close()

    def plot(self, idx: int, t_start: int, t_end: int, s_idx: int):
        fig = plt.figure()
        rows = 2
        columns = t_end - t_start
        for i in range(columns):
            truth = fig.add_subplot(rows, columns, i+1)
            truth.imshow(np.sqrt(np.sum(np.square(self.truth[idx, :, s_idx, :, (t_start+i)//4, 0:3]), axis=-1)))

            pred = fig.add_subplot(rows, columns, i+columns+1)
            pred.imshow(np.sqrt(np.sum(np.square(self.pred[idx, :, s_idx, :, t_start+i, 0:3]), axis=-1)))

        plt.show()


if __name__ == '__main__':
    SAMPLE_IDX = 0
    TIME_IDX = 32
    PATH = '/home/moritz/Documents/fourier_neural_operator/out/experiments/PLAIN_VESSEL_819245/sup_res-820283.npz'
    FILE_PATH = None  # '/home/moritz/Documents/fourier_neural_operator/graphics'
    plotter = EvaluationPlotter(path=PATH, sample_idx=SAMPLE_IDX, time_idx=TIME_IDX, save_path=FILE_PATH)
    plotter.plot_sample(0)
    plotter.plot_sample(1)
    plotter.plot_sample(2)
    plotter.plot_sample(3)