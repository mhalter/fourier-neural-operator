import os

import matplotlib.pyplot as plt
import numpy as np
from os.path import join
from data import TorchDataset
from subprocess import call

ELEV = 0
AZIM = 0
ROLL = 0


def scatter(fig, pos, coordinates, value, elev, azim, roll):
    ax = fig.add_subplot(pos, projection='3d')
    sc = ax.scatter(coordinates[..., 0], coordinates[..., 1], coordinates[..., 2], c=value)
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_zlabel('z')
    ax.set_xticks([])
    ax.set_yticks([])
    ax.set_zticks([])
    ax.set_title('u [m/s]')
    ax.view_init(elev=elev, azim=azim, roll=roll)
    fig.colorbar(sc, ax=ax)


def get_selection(grid, U, p):
    mag_U = np.sqrt(np.sum(np.square(U), axis=-1))
    selection_indices = np.where(mag_U != 0.0)
    grid_selection = grid[selection_indices[0], selection_indices[1], selection_indices[2], :]
    U_selection = U[selection_indices[0], selection_indices[1], selection_indices[2], :]
    u_selection = U_selection[..., 0]
    v_selection = U_selection[..., 1]
    w_selection = U_selection[..., 2]
    p_selection = p[selection_indices[0], selection_indices[1], selection_indices[2]]
    return grid_selection, u_selection, v_selection, w_selection, p_selection


def get_flow_statistics(U, p):
    mag_U = np.sqrt(np.sum(np.square(U), axis=-1))
    selection = mag_U != 0.0
    mean_flow = np.mean(mag_U, axis=(0, 1, 2), where=selection)
    mean_pressure = np.mean(p, axis=(0, 1, 2), where=selection)
    return mean_flow, mean_pressure


def add_flow_statistics_plot(ax, t, mean_flow, mean_pressure):
    color = 'tab:blue'
    ax.set_xlabel('time[s]')
    ax.set_ylabel('mean flow velocity [m/s]', color=color)
    ax.plot(t, mean_flow, color=color)
    ax.tick_params(axis='y', labelcolor=color)

    ax = ax.twinx()

    color = 'tab:orange'
    ax.set_ylabel('mean pressure [kPa]', color=color)
    ax.plot(t, mean_pressure, color=color)
    ax.tick_params(axis='y', labelcolor=color)


class AbstractPlotter:
    def __init__(self, path: str, sample_idx: int, save_path: str):
        self.dataset = TorchDataset(path=path, sample_rate=4, encoding='MIXED_ENC')
        self.save_path = save_path
        self.sample_idx = sample_idx
        self.elev = ELEV
        self.azim = AZIM
        self.roll = ROLL

    def set_view(self, elev: float, azim: float, roll: float):
        self.elev = elev
        self.azim = azim
        self.roll = roll

    def get_sample(self):
        c_idx, s_idx = self.dataset.case_index_list[self.sample_idx]
        data = np.load(self.dataset.cases[c_idx]['path'])
        grid = data['grid'][s_idx, ...]
        values = data['data'][s_idx, ...]
        print(values.shape)
        bnd_enc = data['boundary_encoding'][s_idx, ...]
        t = np.array([float(v) for v in data['t'][s_idx, ...]])
        return t - t[0], grid, values, bnd_enc


class DatasetPlotter(AbstractPlotter):
    def __init__(self, path: str, sample_idx: int, time_idx: int, save_path: str = None):
        super().__init__(path, sample_idx, save_path)
        self.time_idx = time_idx

    def plot_flow_statistics(self):
        plt.rcParams.update({'font.size': 22})
        t, grid, values, bnd_enc = self.get_sample()
        U = values[..., :3]
        p = values[..., 3] * 0.994
        mean_flow, mean_pressure = get_flow_statistics(U, p)

        fig, ax = plt.subplots(figsize=(30, 15))
        add_flow_statistics_plot(ax, t, mean_flow, mean_pressure)
        fig.tight_layout()

        if self.save_path is not None:
            plt.savefig(join(self.save_path, 'flow_statistics'))
            plt.close()
        else:
            plt.show()

    def plot_haemodynamic_features(self):
        plt.rcParams.update({'font.size': 22})
        t, grid, values, bnd_enc = self.get_sample()
        U = values[..., self.time_idx, :3]
        p = values[..., self.time_idx, 3] * 0.994
        grid_selection, u_selection, v_selection, w_selection, p_selection = get_selection(grid, U, p)
        fig = plt.figure(figsize=(50, 10))

        scatter(fig, 141, grid_selection, u_selection, self.azim, self.elev, self.roll)
        scatter(fig, 142, grid_selection, v_selection, self.azim, self.elev, self.roll)
        scatter(fig, 143, grid_selection, w_selection, self.azim, self.elev, self.roll)
        scatter(fig, 144, grid_selection, p_selection, self.azim, self.elev, self.roll)

        if self.save_path is not None:
            plt.savefig(join(self.save_path, 'features_haemodynamic_parameters'))
            plt.close()
        else:
            plt.show()

    def plot_geometric_features(self):
        plt.rcParams.update({'font.size': 22})
        t, grid, values, bnd_enc = self.get_sample()
        U = values[..., self.time_idx, :3]
        mag_U = np.sqrt(np.sum(np.square(U), axis=-1))
        selection_indices = np.where(mag_U != 0.0)
        inverted_selection_indices = np.where(mag_U == 0.0)
        grid_selection = grid[selection_indices[0], selection_indices[1], selection_indices[2], :]
        dist_selection = bnd_enc[selection_indices[0], selection_indices[1], selection_indices[2], 1]
        dist_inverted_selection = bnd_enc[inverted_selection_indices[0], inverted_selection_indices[1], inverted_selection_indices[2], 1]
        grid_inverted_selection = grid[inverted_selection_indices[0], inverted_selection_indices[1], inverted_selection_indices[2]]

        fig = plt.figure(figsize=(25, 10))

        scatter(fig, 121, grid_selection, dist_selection, self.azim, self.elev, self.roll)
        scatter(fig, 122, grid_inverted_selection, dist_inverted_selection, self.azim, self.elev, self.roll)

        if self.save_path is not None:
            plt.savefig(join(self.save_path, 'features_geometric_parameters'))
            plt.close()
        else:
            plt.show()

    def plot_distance_vs_mag_U(self):
        plt.rcParams.update({'font.size': 28})
        plt.figure(figsize=(30, 30))
        t, grid, values, bnd_enc = self.get_sample()
        U = values[..., self.time_idx, :3]
        p = values[..., self.time_idx, 3]
        mag_U = np.sqrt(np.sum(np.square(U), axis=-1))
        selection_indices = np.where(mag_U != 0.0)
        grid_selection, u_selection, v_selection, w_selection, p_selection = get_selection(grid, U, p)
        mag_U_selection = np.sqrt(np.square(u_selection) + np.square(v_selection) + np.square(w_selection))
        dist_selection = bnd_enc[selection_indices[0], selection_indices[1], selection_indices[2], 1]
        dist_mean = np.mean(dist_selection)
        mag_U_mean = np.mean(mag_U_selection)

        r = np.sum((mag_U_selection - mag_U_mean) * (dist_selection - dist_mean)) / \
            np.sqrt(np.sum(np.square(mag_U_selection - mag_U_mean)) * np.sum(np.square(dist_selection - dist_mean)))
        print(f'r: {r}')

        plt.scatter(dist_selection * 1e3, mag_U_selection)
        plt.xlabel('distance from wall [mm]')
        plt.ylabel('absolute flow velocity [m/s]')
        if self.save_path is not None:
            plt.savefig(join(self.save_path, 'mag_u_vs_dist'))
            plt.close()
        else:
            plt.show()


class DatasetAnimatedPlotter(AbstractPlotter):
    def __init__(self, path: str, sample_idx: int, save_path: str = None):
        super().__init__(path, sample_idx, save_path)

    def plot(self):
        plt.rcParams.update({'font.size': 23})
        time, grid, values, bnd_enc = self.get_sample()
        U = values[..., :3]
        p = values[..., 3] * 0.994
        mean_flow, mean_pressure = get_flow_statistics(U, p)
        min_mean_flow = min(mean_flow)
        max_mean_flow = max(mean_flow)
        save_dir = None if self.save_path is None else join(self.save_path, 'frames')
        if save_dir is not None:
            if not os.path.isdir(save_dir):
                os.mkdir(save_dir)
        for i in range(len(time)):
            fig = plt.figure(figsize=(45, 30))

            ax = fig.add_subplot(236)
            add_flow_statistics_plot(ax, time, mean_flow, mean_pressure)
            ax.vlines(time[i], min_mean_flow, max_mean_flow, colors='tab:red')
            grid_selection, u_selection, v_selection, w_selection, p_selection = get_selection(grid, U[..., i, :], p[..., i])
            scatter(fig, 231, grid_selection,  u_selection, self.azim, self.elev, self.roll)
            scatter(fig, 232, grid_selection,  v_selection, self.azim, self.elev, self.roll)
            scatter(fig, 233, grid_selection,  w_selection, self.azim, self.elev, self.roll)
            scatter(fig, 234, grid_selection,  p_selection, self.azim, self.elev, self.roll)

            if save_dir is not None:
                plt.savefig(join(save_dir, 'frame%02d.png' % i))
                plt.close()
            else:
                plt.show()
                break
        if save_dir is not None:
            os.chdir(save_dir)
            call(['ffmpeg', '-framerate', '10', '-i', 'frame%02d.png', '-r', '30', '-pix_fmt', 'yuv420p', join(self.save_path, 'hemodynamic_parameters_animtated.mp4')])
            # rmtree(save_dir)


if __name__ == '__main__':
    PATH = '/home/moritz/Documents/fourier_neural_operator/data/numpy/pipe_selection_v2'
    SAMPLE_IDX = 0
    TIME_IDX = 10
    T = 50
    FILE_PATH = None
    # FILE_PATH = '/home/moritz/Documents/fourier_neural_operator/graphics'

    # plotter = DatasetAnimatedPlotter(path=PATH, sample_idx=SAMPLE_IDX, save_path=FILE_PATH)
    # plotter.set_view(45, 10, 0)
    # plotter.plot()
    # exit()

    plotter = DatasetPlotter(path=PATH, sample_idx=SAMPLE_IDX, time_idx=TIME_IDX, save_path=FILE_PATH)
    plotter.set_view(0, 0, 0)
    plotter.plot_flow_statistics()
    plotter.plot_haemodynamic_features()
    plotter.plot_geometric_features()
    plotter.plot_distance_vs_mag_U()
