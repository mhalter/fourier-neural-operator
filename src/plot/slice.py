from numpy import ndarray
from matplotlib.pyplot import Figure


class SlicePlotter:
    def __init__(self, fig: Figure, pos: int, x_idx: int = 0, y_idx: int = 0, z_idx: int = 0, color_bar: bool = False,
                 title: str = '',  x_label: str = 'x', y_label: str = 'y', z_label: str = 'z'):
        self.fig = fig
        self.x_cut = fig.add_subplot(pos)
        self.y_cut = fig.add_subplot(pos+1)
        self.z_cut = fig.add_subplot(pos+2)
        self.y_cut.set_title(title)
        self.x_cut.set_xlabel(y_label)
        self.x_cut.set_ylabel(z_label)
        self.y_cut.set_xlabel(x_label)
        self.y_cut.set_ylabel(z_label)
        self.z_cut.set_xlabel(x_label)
        self.z_cut.set_ylabel(y_label)
        self.color_bar = color_bar
        self.x_idx = x_idx
        self.y_idx = y_idx
        self.z_idx = z_idx

    def imshow(self, data: ndarray):
        x_cut_data = data[self.x_idx, :, :]
        y_cut_data = data[:, self.y_idx, :]
        z_cut_data = data[:, :, self.z_idx]
        pos_x = self.x_cut.imshow(x_cut_data)
        pos_y = self.y_cut.imshow(y_cut_data)
        pos_z = self.z_cut.imshow(z_cut_data)
        if self.color_bar:
            self.fig.colorbar(pos_x, ax=self.x_cut)
            self.fig.colorbar(pos_y, ax=self.y_cut)
            self.fig.colorbar(pos_z, ax=self.z_cut)
