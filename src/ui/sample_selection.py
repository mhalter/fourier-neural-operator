import os
from typing import Tuple, List, Union, Dict
from vtk import (
    vtkActor,
    vtkDataSetMapper,
    VTK_FONT_FILE,
    vtkLookupTable,
    vtkRenderer,
    vtkRenderWindow,
    vtkRenderWindowInteractor,
    vtkTextActor,
    vtkTextProperty,
    vtkTextRepresentation,
    vtkTextWidget
)

from ui.colors import colors
from utils.vtk_helper import KeyPressStyle
from data import VtkSample
from utils.filesystem_helper import get_vtk_structure_files
from utils.csv_helper import CsvFile
from utils.math import shift_with_rollover

FONT_FILE = '/home/moritz/Documents/fourier_neural_operator/src/fonts/nk57-monospace/nk57-monospace-cd-bd.ttf'

UI_DEF = [
    {
        'type': 'vtkTextActor',
        'name': 'LOADING_ACTOR',
        'pos1': (0.0, 0.0),
        'pos2': (1.0, 0.1),
        'placeholder': 'Loading - Case [ 0/ 0] - Sample [ 0/ 0]'
    }, {
        'type': 'vtkTextActor',
        'name': 'CASE_NAME_ACTOR',
        'pos1': (0.0, 0.95),
        'pos2': (0.1, 0.05),
        'placeholder': '      - [ 0/ 0]'
    }, {
        'type': 'vtkTextActor',
        'name': 'SAMPLE_SELECTION_ACTOR',
        'pos1': (0.9, 0.95),
        'pos2': (0.1, 0.05),
        'placeholder': 'Sample  [ 0/ 0]'
    }, {
        'type': 'vtkActor',
        'name': 'VESSEL_ACTOR',
        'color': 'White',
        'opacity': 1.0
    }, {
        'type': 'vtkActor',
        'name': 'GRID_ACTOR',
        'color': 'Black',
        'opacity': 1.0
    }
]


class SampleSelectionUI(vtkRenderer):
    def __init__(self, N: int, structure_path: str, csv_path: str, case_idx: int = 0):
        super(vtkRenderer).__init__()
        self.text_actors: Dict[str, vtkTextActor] = {}
        self.actors: Dict[str, vtkActor] = {}
        self.mappers: Dict[str, vtkDataSetMapper] = {}
        self.text_widgets: List[vtkTextWidget] = []
        self.dataset = VtkSample(N)
        self.cases = get_vtk_structure_files(structure_path)
        self.case_idx = case_idx
        self.csv_path = csv_path
        self.csv = CsvFile()
        self.sample_idx = 0
        self.opaque_grid = False
        self.op = 0
        self.mod1 = False
        self.mod2 = False

        rw = vtkRenderWindow()
        rw.AddRenderer(self)
        rw.SetWindowName('BaseUI')

        rw = vtkRenderWindow()
        rw.AddRenderer(self)
        rw.SetWindowName('UI')

        self.interactor = vtkRenderWindowInteractor()
        self.interactor.SetRenderWindow(rw)
        self.interactor.SetInteractorStyle(KeyPressStyle(self.key_event_handler))

        self.SetBackground(colors.GetColor3d('bkg'))
        self.actor_list_definitions = {}

        for a_dict in UI_DEF:
            if a_dict['type'] == 'vtkTextActor':
                self.add_text_actor(a_dict['name'], a_dict['pos1'], a_dict['pos2'], a_dict['placeholder'])
            elif a_dict['type'] == 'vtkActor':
                self.add_actor(a_dict['name'], a_dict['color'], a_dict['opacity'])

        self.mappers['VESSEL_ACTOR'].SetInputConnection(self.dataset.get_source('READER'))
        self.text_actors['LOADING_ACTOR'].VisibilityOff()
        if os.path.isfile(csv_path):
            self.csv.load(csv_path)
        self.load_case(case_idx)

    def start(self):
        self.interactor.Initialize()
        self.Render()
        for w in self.text_widgets:
            w.On()
        self.Render()
        self.interactor.Start()

    def add_text_actor(self, name: str, pos1: Tuple[float, float], pos2: Tuple[float, float], placeholder: str = '',
                       color: str = 'DarkSlateGrey'):
        p = vtkTextProperty()
        p.SetColor(colors.GetColor3d(color))
        p.SetFontFamily(VTK_FONT_FILE)
        p.SetFontFile(FONT_FILE)
        p.SetLineSpacing(1.0)
        p.ShadowOff()

        r = vtkTextRepresentation()
        r.GetPositionCoordinate().SetValue(*pos1)
        r.GetPosition2Coordinate().SetValue(*pos2)

        a = vtkTextActor()
        a.SetTextProperty(p)
        a.SetInput(placeholder)
        a.UseBorderAlignOn()

        w = vtkTextWidget()
        w.SetRepresentation(r)
        w.SetInteractor(self.interactor)
        w.SetTextActor(a)
        w.SelectableOn()
        w.EnabledOff()

        self.AddActor(a)
        self.text_actors[name] = a
        self.text_widgets.append(w)

    def add_actor(self, name: str, color: Union[str, vtkLookupTable], opacity: float = 1.0):
        m = vtkDataSetMapper()

        a = vtkActor()
        a.SetMapper(m)

        if isinstance(color, vtkLookupTable):
            m.SetLookupTable(color)
            m.ScalarVisibilityOn()
        else:
            a.GetProperty().SetColor(colors.GetColor3d(color))

        a.GetProperty().SetOpacity(opacity)
        a.VisibilityOff()
        self.AddActor(a)
        self.actors[name] = a
        self.mappers[name] = m

    def key_event_handler(self, obj, event):
        key = obj.GetInteractor().GetKeySym()
        self.center_camera()
        if key == 'Escape':
            exit(0)
        elif key == 'Return':
            self.csv.save(self.csv_path)
            exit(0)
        elif key == 'Control_L':
            self.mod2 = False
            self.mod1 = not self.mod1
        elif key == 'Alt_L':
            self.mod1 = False
            self.mod2 = not self.mod2
        elif key == 'n':
            self.new_sample()
        elif key in ['Up', 'Down']:
            self.shift_sample_idx(1 if key == 'Down' else -1)
        elif key in ['Left', 'Right']:
            self.load_case(shift_with_rollover(self.case_idx, 1 if key == 'Right' else -1, len(self.cases)))
        elif not self.sample_idx == 0:
            if key == 'd':
                self.copy_sample()
            elif key == 'x':
                self.remove_sample()
            elif key in ['i', 'k']:
                self.change_translation(1 if key == 'i' else -1, 0, self.op)
            elif key in ['j', 'l']:
                self.change_translation(1 if key == 'l' else -1, 1, self.op)
            elif key in ['u', 'o']:
                self.change_translation(1 if key == 'o' else -1, 2, self.op)
            elif key in ['r', 't']:
                self.op = shift_with_rollover(self.op, 1 if key == 't' else -1, 3)
            elif key in ['comma', 'period']:
                self.change_translation(1 if key == 'period' else -1, 0, 2)
                self.change_translation(1 if key == 'period' else -1, 1, 2)
                self.change_translation(1 if key == 'period' else -1, 2, 2)
        self.GetRenderWindow().Render()

    def change_translation(self, change: float, axis: int, op: int):
        idx = ([-1] + self.csv.find_rows(self.cases[self.case_idx][1]))[self.sample_idx]
        if op == 0:
            change *= 0.01 * (0.1 if self.mod1 else 1.0) * (0.01 if self.mod2 else 1.0)
            if axis == 0:
                self.csv[idx].translate_x += change
            elif axis == 1:
                self.csv[idx].translate_y += change
            elif axis == 2:
                self.csv[idx].translate_z += change
        elif op == 1:
            change *= 5 * (0.2 if self.mod1 else 1.0) * (0.04 if self.mod2 else 1.0)
            if axis == 0:
                self.csv[idx].rotate_x += change
            elif axis == 1:
                self.csv[idx].rotate_y += change
            elif axis == 2:
                self.csv[idx].rotate_z += change
        elif op == 2:
            change = 1 + 0.1 * change * (0.5 if self.mod1 else 1.0) * (0.25 if self.mod2 else 1.0)
            if axis == 0:
                s = self.csv[idx].scale_x
                s_new = s * change
                self.csv[idx].scale_x = s_new
                t = (s_new - s) / 2
                self.csv[idx].translate_x += t
            elif axis == 1:
                s = self.csv[idx].scale_y
                s_new = s * change
                self.csv[idx].scale_y = s_new
                t = (s_new - s) / 2
                self.csv[idx].translate_y += t
            elif axis == 2:
                s = self.csv[idx].scale_z
                s_new = s * change
                self.csv[idx].scale_z = s_new
                t = (s_new - s) / 2
                self.csv[idx].translate_z += t
        self.dataset.set_transformation(self.csv[idx])
        self.mappers['GRID_ACTOR'].SetInputData(self.dataset.get_source('RESAMPLER'))

    def load_case(self, idx):
        self.actors['GRID_ACTOR'].VisibilityOff()

        self.case_idx = idx
        self.sample_idx = 0
        case = self.cases[idx]
        samples = self.csv.find_rows(case[1])
        self.dataset.load_preview(case[0])

        self.text_actors['CASE_NAME_ACTOR'].SetInput(f'{case[1]} - [{idx + 1:>2}/{len(self.cases):>2}]')
        self.text_actors['SAMPLE_SELECTION_ACTOR'].SetInput(f'Sample  [ -/{len(samples):>2}]')

        self.actors['VESSEL_ACTOR'].GetProperty().SetOpacity(1.0)
        self.actors['VESSEL_ACTOR'].VisibilityOn()

    def shift_sample_idx(self, shift: int):
        samples = [-1] + self.csv.find_rows(self.cases[self.case_idx][1])
        self.sample_idx = shift_with_rollover(self.sample_idx, shift, len(samples))
        if self.sample_idx == 0:
            self.actors['GRID_ACTOR'].VisibilityOff()
            self.actors['VESSEL_ACTOR'].GetProperty().SetOpacity(1.0)
            self.text_actors['SAMPLE_SELECTION_ACTOR'].SetInput(f'Sample  [ -/{len(samples) - 1:>2}]')
        else:
            self.actors['VESSEL_ACTOR'].GetProperty().SetOpacity(1.0 if self.opaque_grid else 0.25)
            self.actors['GRID_ACTOR'].GetProperty().SetOpacity(0.25 if self.opaque_grid else 1.0)
            row = self.csv[samples[self.sample_idx]]
            self.text_actors['SAMPLE_SELECTION_ACTOR'].SetInput(f'Sample  [{self.sample_idx:>2}/{len(samples) - 1:>2}]')
            self.dataset.set_transformation(row)
            self.mappers['GRID_ACTOR'].SetInputData(self.dataset.get_source('RESAMPLER'))
            self.actors['GRID_ACTOR'].VisibilityOn()

    def center_camera(self):
        if self.dataset.resampler.GetOutput() is not None and not self.sample_idx == 0:
            bounds = self.dataset.resampler.GetOutput().GetBounds()
        else:
            bounds = self.dataset.reader.GetOutput().GetBounds()
        self.GetActiveCamera().SetFocalPoint((bounds[0] + bounds[1]) / 2, (bounds[2] + bounds[3]) / 2,
                                             (bounds[4] + bounds[5]) / 2)

    def new_sample(self):
        self.csv.add_new({'case': self.cases[self.case_idx][1]})
        self.switch_to_sample(-1)

    def copy_sample(self):
        idx = ([-1] + self.csv.find_rows(self.cases[self.case_idx][1]))[self.sample_idx]
        self.csv.add_new(self.csv[idx].get_dict())
        self.switch_to_sample(-1)

    def switch_to_sample(self, idx):
        samples = [-1] + self.csv.find_rows(self.cases[self.case_idx][1])
        self.sample_idx = len(samples) - 1
        row = self.csv[idx]
        self.text_actors['SAMPLE_SELECTION_ACTOR'].SetInput(f'Sample  [{self.sample_idx:>2}/{len(samples) - 1:>2}]')
        self.dataset.set_transformation(row)
        self.mappers['GRID_ACTOR'].SetInputData(self.dataset.get_source('RESAMPLER'))
        self.actors['VESSEL_ACTOR'].GetProperty().SetOpacity(1.0 if self.opaque_grid else 0.25)
        self.actors['GRID_ACTOR'].GetProperty().SetOpacity(0.25 if self.opaque_grid else 1.0)
        self.actors['GRID_ACTOR'].VisibilityOn()

    def remove_sample(self):
        self.actors['GRID_ACTOR'].VisibilityOff()
        samples = [-1] + self.csv.find_rows(self.cases[self.case_idx][1])
        idx = samples[self.sample_idx]
        self.sample_idx -= 1
        new_idx = samples[self.sample_idx]
        self.csv.remove(idx)

        row = self.csv[new_idx]
        self.dataset.set_transformation(row)

        self.text_actors['SAMPLE_SELECTION_ACTOR'].SetInput(f'Sample  [{self.sample_idx:>2}/{len(samples) - 2:>2}]')
        self.mappers['GRID_ACTOR'].SetInputData(self.dataset.get_source('RESAMPLER'))
        self.actors['GRID_ACTOR'].VisibilityOn()
