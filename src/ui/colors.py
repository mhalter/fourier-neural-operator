from vtk import vtkNamedColors

COLORS = {
    'bkg': [26, 51, 102, 255]
}

colors = vtkNamedColors()

for key, values in COLORS.items():
    c = map(lambda x: x / 255.0, values)
    colors.SetColor(key, *c)
