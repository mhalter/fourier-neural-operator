import os
from typing import Tuple, List, Union, Dict
from vtk import (
    vtkActor,
    vtkDataSetMapper,
    VTK_FONT_FILE,
    vtkLookupTable,
    vtkRenderer,
    vtkRenderWindow,
    vtkRenderWindowInteractor,
    vtkTextActor,
    vtkTextProperty,
    vtkTextRepresentation,
    vtkTextWidget,
    vtkPNGWriter,
    vtkWindowToImageFilter
)

from ui.colors import colors
from utils.vtk_helper import KeyPressStyle
from data import VtkCase
from utils.filesystem_helper import get_files
from utils.csv_helper import CsvFile
from utils.math import shift_with_rollover

FONT_FILE = '/home/moritz/Documents/fourier_neural_operator/src/fonts/nk57-monospace/nk57-monospace-cd-bd.ttf'

UI_DEF = [
    {
        'type': 'vtkTextActor',
        'name': 'LOADING_ACTOR',
        'pos1': (0.0, 0.0),
        'pos2': (1.0, 0.1),
        'placeholder': 'Loading - Case [ 0/ 0] - Sample [ 0/ 0]'
    }, {
        'type': 'vtkTextActor',
        'name': 'CASE_NAME_ACTOR',
        'pos1': (0.0, 0.95),
        'pos2': (0.1, 0.05),
        'placeholder': '      - [ 0/ 0]'
    }, {
        'type': 'vtkActor',
        'name': 'VESSEL_ACTOR',
        'color': 'White',
        'opacity': 1.0
    }, {
        'type': 'vtkActor',
        'name': 'GRID_ACTOR',
        'color': 'Black',
        'opacity': 1.0
    }
]


class DatasetInspectionUI(vtkRenderer):
    def __init__(self, N: int, structure_path: str, csv_path: str, image_path: str, case_idx: int = 0):
        super(vtkRenderer).__init__()
        self.text_actors: Dict[str, vtkTextActor] = {}
        self.actors: Dict[str, Union[vtkActor, List[vtkActor]]] = {}
        self.mappers: Dict[str, Union[vtkDataSetMapper, List[vtkDataSetMapper]]] = {}
        self.text_widgets: List[vtkTextWidget] = []
        self.dataset = VtkCase(N)
        self.cases = get_files(structure_path,
                               file_extension='vtk',
                               key_patterns={'case': r'(C\d{4})'},
                               sort_pattern=r'(C\d{4})')
        self.case_idx = case_idx
        self.csv_path = csv_path
        self.csv = CsvFile()
        self.opaque_grid = False
        self.op = 0
        self.mod1 = False
        self.mod2 = False
        self.show_all = False

        rw = vtkRenderWindow()
        rw.AddRenderer(self)
        rw.SetWindowName('BaseUI')

        rw = vtkRenderWindow()
        rw.AddRenderer(self)
        rw.SetWindowName('UI')

        self.interactor = vtkRenderWindowInteractor()
        self.interactor.SetRenderWindow(rw)
        self.interactor.SetInteractorStyle(KeyPressStyle(self.key_event_handler))

        image_filter = vtkWindowToImageFilter()
        image_filter.SetInput(rw)
        image_filter.SetScale(1)
        image_filter.ReadFrontBufferOff()
        self.png_writer = vtkPNGWriter()
        self.png_writer.SetFileName(image_path)
        self.png_writer.SetInputConnection(image_filter.GetOutputPort())

        self.SetBackground(colors.GetColor3d('bkg'))
        self.actor_list_definitions = {}
        self.grid_actor_def = None

        for a_dict in UI_DEF:
            if a_dict['name'] == 'GRID_ACTOR':
                self.actors['GRID_ACTOR'] = []
                self.mappers['GRID_ACTOR'] = []
                self.grid_actor_def = a_dict
            elif a_dict['type'] == 'vtkTextActor':
                self.add_text_actor(a_dict['name'], a_dict['pos1'], a_dict['pos2'], a_dict['placeholder'])
            elif a_dict['type'] == 'vtkActor':
                self.add_actor(a_dict['name'], a_dict['color'], a_dict['opacity'])

        self.mappers['VESSEL_ACTOR'].SetInputConnection(self.dataset.get_reader_output())
        self.text_actors['LOADING_ACTOR'].VisibilityOff()
        if os.path.isfile(csv_path):
            self.csv.load(csv_path)
        self.load_case(case_idx)

    def start(self):
        self.interactor.Initialize()
        self.Render()
        for w in self.text_widgets:
            w.On()
        self.Render()
        self.interactor.Start()

    def add_text_actor(self, name: str, pos1: Tuple[float, float], pos2: Tuple[float, float], placeholder: str = '',
                       color: str = 'DarkSlateGrey'):
        p = vtkTextProperty()
        p.SetColor(colors.GetColor3d(color))
        p.SetFontFamily(VTK_FONT_FILE)
        p.SetFontFile(FONT_FILE)
        p.SetLineSpacing(1.0)
        p.ShadowOff()

        r = vtkTextRepresentation()
        r.GetPositionCoordinate().SetValue(*pos1)
        r.GetPosition2Coordinate().SetValue(*pos2)

        a = vtkTextActor()
        a.SetTextProperty(p)
        a.SetInput(placeholder)
        a.UseBorderAlignOn()

        w = vtkTextWidget()
        w.SetRepresentation(r)
        w.SetInteractor(self.interactor)
        w.SetTextActor(a)
        w.SelectableOn()
        w.EnabledOff()

        self.AddActor(a)
        self.text_actors[name] = a
        self.text_widgets.append(w)

    def add_actor(self, name: str, color: Union[str, vtkLookupTable], opacity: float = 1.0):
        m = vtkDataSetMapper()

        a = vtkActor()
        a.SetMapper(m)

        if isinstance(color, vtkLookupTable):
            m.SetLookupTable(color)
            m.ScalarVisibilityOn()
        else:
            a.GetProperty().SetColor(colors.GetColor3d(color))

        a.GetProperty().SetOpacity(opacity)
        a.VisibilityOff()
        self.AddActor(a)
        if name == 'GRID_ACTOR':
            self.actors[name].append(a)
            self.mappers[name].append(m)
        else:
            self.actors[name] = a
            self.mappers[name] = m

    def key_event_handler(self, obj, event):
        key = obj.GetInteractor().GetKeySym()
        if key == 's':
            self.png_writer.Write()
        elif key in ['Left', 'Right']:
            self.load_case(shift_with_rollover(self.case_idx, 1 if key == 'Right' else -1, len(self.cases)))

    def load_case(self, idx):
        for actor in self.actors['GRID_ACTOR']:
            self.RemoveActor(actor)

        self.actors['GRID_ACTOR'] = []
        self.mappers['GRID_ACTOR'] = []

        self.case_idx = idx
        case = self.cases[idx]
        samples = [self.csv[i] for i in self.csv.find_rows(case['case'])]
        self.dataset.load_case(case['path'], samples)

        self.text_actors['CASE_NAME_ACTOR'].SetInput(f'{case['case']} - [{idx + 1:>2}/{len(self.cases):>2}]')

        self.actors['VESSEL_ACTOR'].GetProperty().SetOpacity(1.0)
        self.actors['VESSEL_ACTOR'].VisibilityOn()

        for resampler_output in self.dataset.get_resampler_outputs():
            self.add_actor('GRID_ACTOR', self.grid_actor_def['color'], self.grid_actor_def['opacity'])
            self.mappers['GRID_ACTOR'][-1].SetInputData(resampler_output)
            self.actors['GRID_ACTOR'][-1].GetProperty().SetOpacity(1.0)
            self.actors['GRID_ACTOR'][-1].VisibilityOn()
