import random
import numpy as np
from os.path import join, basename
from datetime import datetime
from logging import getLogger

from scipy.io import savemat
from torch import device, no_grad, zeros, Generator, manual_seed
from torch.optim import Adam
from torch.optim.lr_scheduler import StepLR
from torch.cuda import is_available
from torch.utils.data import DataLoader, random_split, Subset

from typing import Union, List
from data import TorchDataset
from utils.logger import log_batch, log_epoch
from nn.layers import Net3d
from nn.loss import LpLoss

LOG_FREQ = 10


class SuperResolutionTask(object):
    def __init__(self, dataset: TorchDataset, out: str, job_id: int, log_freq: int, model: Union[str, None],
                 seed: int = 1234, n_modes: int = 12, eval_idx_list: List[int] = None, batch_size: int = 1):
        self.logger = getLogger('SuperResolutionTask')
        super(SuperResolutionTask, self).__init__()
        self.job_name = 'sup_res-' + str(job_id)
        self.batch_size = batch_size
        self.logger.info('cuda enabled: {}'.format(is_available()))
        self.device = device('cpu') if not is_available() else device('cuda')

        self.logger.info(f'seed : {seed}')
        random.seed(seed)
        np.random.seed(seed)
        manual_seed(seed)

        generator = Generator().manual_seed(seed)

        if eval_idx_list is None:
            train_split, test_split = random_split(dataset, [0.9, 0.1], generator)
        else:
            train_set = set(eval_idx_list)
            test_set = set(range(len(dataset))) - train_set
            test_split = Subset(dataset, list(test_set))
            train_split = Subset(dataset, list(train_set))

        self.output_size = dataset.output_size
        self.logger.info(f'Using {n_modes} Fourier modes.')
        self.net = Net3d(n_modes, 16, 4, dataset.in_features).to(self.device)
        self.train_loader = DataLoader(train_split, batch_size=self.batch_size, shuffle=True, num_workers=3)
        self.test_loader = DataLoader(test_split, batch_size=1, shuffle=False, num_workers=3)

        self.loss = LpLoss(size_average=False)
        self.optimizer = Adam(self.net.parameters())
        self.out = out
        self.log_freq = log_freq

        if model is not None:
            self.logger.info(f'existing model ({basename(model)})')
            opt_dict, self.epoch = self.net.load(model, map_location=self.device)
            # self.optimizer.load_state_dict(opt_dict)
        else:
            self.logger.info('new model')
            self.epoch = 0

        self.scheduler = StepLR(self.optimizer, step_size=100, gamma=0.5)

    def train(self, time: int = 5):
        self.logger.info(f'start training for {time}h with batch size {self.batch_size}')
        start = datetime.now()
        now = start
        next_save = 1
        e = self.epoch + 1
        final_loss = None
        best_loss = 1e5
        while (now - start).total_seconds() < time * 3600:
            e_start = datetime.now()
            b = 1
            training_loss = zeros([1], device=self.device)
            for x, y, _ in self.train_loader:
                b_start = datetime.now()
                x = x.to(self.device)
                y = y.to(self.device)
                loss = self.loss(self.net(x).reshape(self.batch_size, -1), y.reshape(self.batch_size, -1))
                self.optimizer.zero_grad()
                loss.backward()
                self.optimizer.step()
                if b % self.log_freq == 0:
                    log_batch(b, loss.item(), datetime.now() - b_start, self.logger)
                training_loss = training_loss + loss
                b += 1

            self.net.eval()
            test_loss = zeros([1], device=self.device)
            with no_grad():
                for x, y, idx in self.test_loader:
                    x = x.to(self.device)
                    y = y.to(self.device)

                    test_loss = test_loss + self.loss(self.net(x).reshape(self.batch_size, -1),
                                                      y.reshape(self.batch_size, -1)).to(self.device)
            log_epoch(e, test_loss.item() / len(self.test_loader), training_loss.item() / len(self.train_loader), datetime.now() - e_start, self.logger)
            now = datetime.now()
            if loss < best_loss:
                self.net.save(self.optimizer.state_dict(), e, loss,
                              join(self.out, 'models', '{}-best'.format(self.job_name)))
                best_loss = loss.item()
            self.scheduler.step()
            e += 1
            final_loss = loss
        self.net.save(self.optimizer.state_dict(), e, final_loss,
                      join(self.out, 'models', '{}-final'.format(self.job_name, next_save)))
        self.logger.info('training done')
        eval_idx_list = []
        for _, _, idx in self.test_loader:
            eval_idx_list.append(idx)
        self.logger.info(f'evaluated samples {eval_idx_list}')
        self.epoch = e

    def test(self):
        self.net.eval()
        pred = zeros((len(self.test_loader.dataset),) + self.output_size)
        truth = zeros((len(self.test_loader.dataset),) + self.output_size)
        eval_idx_list = []
        with no_grad():
            e_start = datetime.now()
            total_loss = zeros([1], device=self.device)
            b = 0
            for x, y, idx in self.test_loader:
                b_start = datetime.now()
                x = x.to(self.device)
                y = y.to(self.device)

                out = self.net(x)
                loss = self.loss(out.reshape(1, -1), y.reshape(1, -1)).to(self.device)
                pred[b, ...] = out
                truth[b, ...] = y
                b += 1
                log_batch(b, loss.item(), datetime.now() - b_start, self.logger)
                total_loss += loss
                eval_idx_list.append(idx)

            log_epoch(self.epoch - 1, total_loss.item() / len(self.test_loader), 0, datetime.now() - e_start, self.logger)

        self.logger.info('testing done')
        self.logger.info(f'evaluated samples {eval_idx_list}')

        savemat(join(self.out, 'eval', '{}.npz'.format(self.job_name)),
                mdict={'pred': pred.cpu().numpy(), 'truth': truth.cpu().numpy()})


if __name__ == '__main__':
    from argparse import ArgumentParser
    from utils.logger import set_config

    parser = ArgumentParser()
    parser.add_argument('-i', '--id', default=None)
    parser.add_argument('-s', '--src', required=True)
    parser.add_argument('-d', '--dest', required=True)
    parser.add_argument('-l', '--log', default=None)
    parser.add_argument('-v', '--verbosity', action='store_true')
    parser.add_argument('-m', '--model', default=None)
    parser.add_argument('-t', '--training-time', type=float, default=24)
    parser.add_argument('-r', '--sample-rate', type=int, default=10)
    parser.add_argument('-e', '--dataset-type', type=str,
                        choices=['PLAIN', 'MASK_ENC', 'DIST_ENC', 'SQUARE_ENC', 'MIX_ENC'], default='PLAIN')
    parser.add_argument('-x', '--eval', action='store_true')
    parser.add_argument('-n', '--n-modes', default=4, type=int)
    parser.add_argument('-b', '--batch-size', default=4, type=int)

    args = parser.parse_args()
    log_frequency = 1 if args.verbosity else LOG_FREQ
    log_file = join(args.log, f'sup_res-{args.id}.log' if not args.eval else f'eval_sup_res-{args.id}.log')
    level = 'DEBUG' if args.verbosity else 'INFO'
    set_config(level, log_file)
    ds = TorchDataset(path=args.src, sample_rate=args.sample_rate, encoding=args.dataset_type)
    task = SuperResolutionTask(ds, args.dest, args.id, log_frequency, args.model,
                               n_modes=args.n_modes, batch_size=args.batch_size)

    if not args.eval:
        task.train(args.training_time)

    task.test()
