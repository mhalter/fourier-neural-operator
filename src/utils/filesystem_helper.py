import os
import re
import tarfile
from typing import List, Tuple, Callable, Union, Dict
from utils.regex import extract_capture_group


def get_sorted_matches(path: str, pattern: str, key: Union[Callable, None]) -> List[Tuple[str, str]]:
    matches = [(f, re.search(pattern, f)) for f in os.listdir(path)]
    return sorted([(os.path.join(path, f[0]), *f[1].groups()) for f in matches if f[1] is not None])


def get_vtk_file_series(path: str) -> List[Tuple[str, str]]:
    return get_sorted_matches(path, r'_(\d+\.\d+)\.vtk$', lambda x: x[1])


def get_vtk_structure_files(path: str) -> List[Tuple[str, str]]:
    return get_sorted_matches(path, r'^(C\d{4}).vtk$', lambda x: x[1])


def get_numpy_files(path: str) -> List[Tuple[str, str]]:
    return get_sorted_matches(path, r'^C\d{4}_N(\d+).npz', lambda x: x[0])


def extract_vtk(tar_file: str, dest: str, as_series: bool = True) -> None:
    with tarfile.open(tar_file, 'r') as file:
        if as_series:
            file.extractall(dest)
        else:
            member = file.getmembers()[1]
            file.extract(member, dest)


def get_files(path: str,
              file_extension: str = None,
              key_patterns: Dict[str, str] = None,
              sort_pattern: str = None) -> List[Dict[str, str]]:
    files = os.listdir(path)
    if file_extension is not None:
        files = [{'name': f,
                  'path': os.path.join(path, f)} for f in files if re.search(r'\.{}$'.format(file_extension), f)]
    if key_patterns is not None:
        for f in files:
            for key, pattern in key_patterns.items():
                f[key] = extract_capture_group(f['name'], pattern)
    if sort_pattern is not None:
        files = sorted(files, key=lambda f: extract_capture_group(f['name'], sort_pattern))
    return files
