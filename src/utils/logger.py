import os
import logging
from logging import getLogger
from logging.config import dictConfig
from typing import List
from datetime import timedelta
from torch import Tensor

LEVELS: List[int] = [logging.DEBUG, logging.INFO, logging.WARNING, logging.ERROR, logging.FATAL]


def set_config(level: str, filename: str):
    config_dict = {
            'version': 1,
            'formatters': {
                'formatter_0': {
                    'format': '%(levelname)s %(name)s [%(asctime)s] %(message)s'
                }
            },
            'handlers': {
                'handler_0': {
                    'class': 'logging.FileHandler',
                    'formatter': 'formatter_0',
                    'level': level,
                    'filename': filename
                }
            },
            'loggers': {
                '': {
                    'level': level,
                    'handlers': ['handler_0']
                },
                'logger_0': {
                    'level': level,
                    'handler': ['handler_0']
                }
            }
        }
    dictConfig(config_dict)


def log_batch(num: int, loss: float, delta_t: timedelta, log: logging.Logger) -> None:
    h, r = divmod(delta_t.seconds, 3600)
    m, r = divmod(r, 60)
    log.info('Batch {:>3d}, loss: {:.2e}, duration: {:0>2d}:{:0>2}.{}'.format(num, loss, m, r,
                                                                              delta_t.microseconds))


def log_epoch(num: int, test_loss: float, train_loss: float, delta_t: timedelta, log: logging.Logger) -> None:
    h, r = divmod(delta_t.seconds, 3600)
    m, r = divmod(r, 60)
    h += 24 * delta_t.days
    log.info('')
    log.info('Epoch {:>4d}, test_loss: {:.2e}, train_loss: {:.2e}, duration: {:>2d}:{:0>2d}:{:0>2d}'.format(num, test_loss, train_loss, h, m, r))
    log.info('')


if __name__ == '__main__':
    set_config('DEBUG', '/home/moritz/Documents/fourier_neural_operator/log/logger.log')
    logger = getLogger(__name__)
    logger.debug('debug message')
    logger.info('info message')
    logger.error('error message')
    logger.critical('critical message')
