from typing import Tuple, Callable
from vtk import (
    vtkColorSeries,
    vtkNamedColors,
    vtkOutlineFilter,
    vtkPointData,
    vtkTransform,
    vtkPNGWriter,
    vtkTextProperty,
    vtkWindowToImageFilter,
    vtkRenderer,
    vtkRenderWindow,
    vtkRenderWindowInteractor,
    vtkPassArrays,
    vtkDataObject,
    vtkUniformGrid,
    vtkColor3d,
    vtkPointSetAlgorithm,
    vtkAxesActor,
    vtkOrientationMarkerWidget,
    vtkStructuredGridGeometryFilter,
    VTK_FONT_FILE,
    vtkInteractorObserver,
    vtkInteractorStyleTrackballCamera
)
from vtk.util.numpy_support import vtk_to_numpy
from numpy import ndarray

from utils.csv_helper import CsvRow

FONT_FILE= '/fonts/nk57-monospace/nk57-monospace-cd-bd.ttf'


class KeyPressStyle(vtkInteractorStyleTrackballCamera, vtkInteractorObserver):
    def __init__(self, on_key_press: Callable):
        super(vtkInteractorStyleTrackballCamera).__init__()
        self.AddObserver('KeyPressEvent', on_key_press)


def get_array(name: str, points: vtkPointData, dtype: str = 'float32') -> ndarray:
    return vtk_to_numpy(points.GetArray(name)).astype(dtype)


def get_render_objects(key_handler: Callable, title: str) -> Tuple[vtkRenderer, vtkRenderWindow,
                                                                   vtkRenderWindowInteractor]:
    r = vtkRenderer()
    rw = vtkRenderWindow()
    i = vtkRenderWindowInteractor()

    rw.AddRenderer(r)
    i.SetRenderWindow(rw)
    i.SetInteractorStyle(KeyPressStyle(key_handler))
    rw.SetWindowName(title)

    return r, rw, i


def get_transform(row: CsvRow) -> vtkTransform:
    t = vtkTransform()
    t.Translate(*row.get_translation())
    x, y, z = row.get_rotation()
    t.RotateX(x)
    t.RotateY(y)
    t.RotateZ(z)
    t.Scale(*row.get_scaling())
    return t


def get_png_wirter(window: vtkRenderWindow) -> vtkPNGWriter:
    w = vtkPNGWriter()
    i = vtkWindowToImageFilter()
    i.SetInputBufferTypeToRGB()
    w.SetInputConnection(i)
    i.SetInput(window)
    return w


def get_normal_uniform_grid(N: int) -> vtkUniformGrid:
    d = 1 / (N - 1)
    g = vtkUniformGrid()
    g.SetOrigin(0, 0, 0)
    g.SetDimensions(N, N, N)
    g.SetSpacing(d, d, d)
    return g


def get_named_colors(**kwargs):
    colors = vtkNamedColors()
    for key, values in kwargs.items():
        c = map(lambda x: x / 255.0, values)
        colors.SetColor(key, *c)
    return colors


def get_text_property(color: vtkColor3d, spacing: float = 1.5, font_size: int = 36) -> vtkTextProperty:
    p = vtkTextProperty()
    p.SetFontFamily(VTK_FONT_FILE)
    p.SetFontFile(FONT_FILE)
    p.ShadowOff()
    p.SetLineSpacing(spacing)
    p.SetFontSize(font_size)
    p.SetColor(color)
    return p


def get_center_point(source: vtkPointSetAlgorithm) -> Tuple[float, float, float]:
    x_min, x_max, y_min, y_max, z_min, z_max = source.GetOutput().GetBounds()
    return (x_max + x_min) / 2, (y_max + y_min) / 2, (z_max + z_min) / 2


def get_outline_filter(source: vtkPointSetAlgorithm) -> vtkOutlineFilter:
    o = vtkOutlineFilter()
    o.SetInputConnection(source.GetOutputPort())
    return o


def get_geometry_filter(source: vtkPointSetAlgorithm) -> vtkStructuredGridGeometryFilter:
    f = vtkStructuredGridGeometryFilter()
    f.SetInputConnection(source.GetOutputPort())
    return f


def get_pass_array_filter() -> vtkPassArrays:
    p = vtkPassArrays()
    p.AddArray(vtkDataObject.POINT, 'p')
    return p


def get_color_series() -> vtkColorSeries:
    c = vtkColorSeries()
    c.SetColorScheme(vtkColorSeries.BREWER_DIVERGING_SPECTRAL_11)
    return c


def add_axes_widget(renderer: vtkRenderer, interactor: vtkRenderWindowInteractor, color: vtkColor3d):
    a = vtkAxesActor()
    w = vtkOrientationMarkerWidget()
    rgba = [0] * 4
    from vtk import vtkNamedColors
    colors = vtkNamedColors()
    colors.GetColor('Carrot', rgba)
    w.SetOutlineColor(rgba[0], rgba[1], rgba[2])
    w.SetOrientationMarker(a)
    w.SetInteractor(interactor)
    w.SetViewport(0.0, 0.0, 0.2, 0.2)
    w.SetEnabled(1)
    w.InteractiveOn()

    renderer.GetActiveCamera().Azimuth(50)
    renderer.GetActiveCamera().Elevation(-30)
    renderer.ResetCamera()
