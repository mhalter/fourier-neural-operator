import re

from utils.exceptions import MissingCaptureGroupException


def extract_capture_group(string: str, pattern: str) -> str:
    match = re.search(pattern, string)
    if match is not None:
        try:
            return match.group(1)
        except IndexError:
            raise MissingCaptureGroupException(pattern)
    else:
        return ''
