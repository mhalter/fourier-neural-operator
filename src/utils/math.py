import numpy as np
import torch
from functools import partial


def shift_with_rollover(i: int, shift: int, upper_bound: int) -> int:
    j = i + shift
    return (j if j > 0 else j + upper_bound) % upper_bound


def linear_interpolation(x: np.ndarray, x_a: np.ndarray, x_b: np.ndarray, y_a: np.ndarray,
                         y_b: np.ndarray) -> np.ndarray:
    shape = y_a.shape[:-1] + (1,)
    return (y_a * (x_b - x).reshape(shape) + y_b * (x_a - x).reshape(shape)) / (x_a - x_b).reshape(shape)


def complex_mul4d(a: torch.Tensor, b: torch.Tensor) -> torch.Tensor:
    a_view = torch.view_as_real(a)
    # (batch, in_channel, x,y,z,t ), (in_channel, out_channel, x,y,z,t) -> (batch, out_channel, x,y,z,t)
    op = partial(torch.einsum, "bixyzt,ioxyzt->boxyzt")
    return torch.view_as_complex(torch.stack([
        op(a_view[..., 0], b[..., 0]) - op(a_view[..., 1], b[..., 1]),
        op(a_view[..., 1], b[..., 0]) + op(a_view[..., 0], b[..., 1])
    ], dim=-1))
