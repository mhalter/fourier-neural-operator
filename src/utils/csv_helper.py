import csv
import re
from logging import getLogger
from functools import reduce
from schema import Schema, SchemaError, And
from typing import Dict, Union, List, Tuple

import numpy as np

__logger = getLogger(__name__)


def is_number(s: str) -> bool:
    return bool(re.search(r'^-?\d+(?:\.\d*)?$', s))


row_schema = Schema({
    'case': And(str, lambda c: len(c) == 5, error='case must be a string of length 5'),
    'translate_x': And(str, is_number, error='translate_x must be a number'),
    'translate_y': And(str, is_number, error='translate_y must be a number'),
    'translate_z': And(str, is_number, error='translate_z must be a number'),
    'rotate_x': And(str, is_number, error='rotate_x must be a number'),
    'rotate_y': And(str, is_number, error='rotate_y must be a number'),
    'rotate_z': And(str, is_number, error='rotate_z must be a number'),
    'scale_x': And(str, is_number, error='scale_x must be a number'),
    'scale_y': And(str, is_number, error='scale_y must be a number'),
    'scale_z': And(str, is_number, error='scale_z must be a number')
})


def validate_row(row: Dict[str, Union[str, int, float]]) -> Union[Dict[str, Union[str, int, float]], None]:
    try:
        return row_schema.validate(row)
    except SchemaError as e:
        if 'case' in row and len(row['case']) == 5:
            __logger.info(e)
            return {'case': row['case'],
                    'translate_x': 0.0,
                    'translate_y': 0.0,
                    'translate_z': 0.0,
                    'rotate_x': 0.0,
                    'rotate_y': 0.0,
                    'rotate_z': 0.0,
                    'scale_x': 0.05,
                    'scale_y': 0.05,
                    'scale_z': 0.05}
        else:
            __logger.warning(e)
            return None


class CsvRow:
    def __init__(self, row: Dict[str, str]):
        row = validate_row(row)
        self.case = row['case']
        self.translate_x = float(row['translate_x'])
        self.translate_y = float(row['translate_y'])
        self.translate_z = float(row['translate_z'])
        self.rotate_x = float(row['rotate_x'])
        self.rotate_y = float(row['rotate_y'])
        self.rotate_z = float(row['rotate_z'])
        self.scale_x = float(row['scale_x'])
        self.scale_y = float(row['scale_y'])
        self.scale_z = float(row['scale_z'])

    def __str__(self) -> str:
        return (f'CSVLine(case={self.case}, translate_x={self.translate_x:.3f}, translate_y={self.translate_y:.3f}, '
                f'translate_z={self.translate_z:.3f}, rotate_x={self.rotate_x:.0f}, rotate_y={self.rotate_y:.0f}, '
                f'rotate_z={self.rotate_z:.0f}, scale_x={self.scale_x:.4f}, scale_y={self.scale_y:.4f}, '
                f'scale_z={self.scale_z:.4f}')

    def get_dict(self) -> Dict[str, Union[str, float]]:
        return {
            'case': self.case,
            'translate_x': str(self.translate_x),
            'translate_y': str(self.translate_y),
            'translate_z': str(self.translate_z),
            'rotate_x': str(self.rotate_x),
            'rotate_y': str(self.rotate_y),
            'rotate_z': str(self.rotate_z),
            'scale_x': str(self.scale_x),
            'scale_y': str(self.scale_y),
            'scale_z': str(self.scale_z)
        }

    def get_hash(self) -> int:
        return hash(str(self.translate_x) + str(self.translate_y) + str(self.translate_z) +
                    str(self.rotate_x) + str(self.rotate_y) + str(self.rotate_z) +
                    str(self.scale_x) + str(self.scale_y) + str(self.scale_z))

    def copy(self):
        return CsvRow(self.get_dict())

    def get_translation(self) -> Tuple[float, float, float]:
        return self.translate_x, self.translate_y, self.translate_z

    def get_rotation(self) -> Tuple[float, float, float]:
        return self.rotate_x, self.rotate_y, self.rotate_z

    def get_scaling(self) -> Tuple[float, float, float]:
        return self.scale_x, self.scale_y, self.scale_z


class CsvFile:
    def __init__(self):
        self.rows: List[CsvRow] = []
        self.filter = np.empty((0,))

    def update_filter(self, items: List[int]):
        arr = np.asarray(items)
        if self.filter.shape[0] > 0:
            arr = np.concatenate([arr, self.filter])
        self.filter = np.unique(arr)

    def load(self, path: str) -> None:
        with open(path, 'r') as file:
            reader = csv.DictReader(file, delimiter=';')
            self.rows = [CsvRow(r) for r in reader]

    def save(self, path: str) -> None:
        with open(path, 'w', newline='') as file:
            writer = csv.DictWriter(file, delimiter=';', fieldnames=['case', 'translate_x', 'translate_y',
                                                                     'translate_z', 'rotate_x', 'rotate_y', 'rotate_z',
                                                                     'scale_x', 'scale_y', 'scale_z'])
            writer.writeheader()
            writer.writerows([r.get_dict() for r in sorted(self.rows, key=lambda r: r.case)])

    def __getitem__(self, item: int) -> CsvRow:
        mask = np.ones(len(self.rows), np.bool_)
        if self.filter.shape[0] > 0:
            mask[self.filter] = 0
        avail = np.arange(len(self.rows))[mask]
        return self.rows[avail[item]]

    def __len__(self) -> int:
        return len(self.rows) - self.filter.shape[0]

    def __str__(self) -> str:
        return '\n'.join(str(r) for r in self.rows)

    def add_new(self, row_dict: Dict[str, Union[str, float]]) -> None:
        self.rows.append(CsvRow(row_dict))

    def find_rows(self, case: str) -> List[int]:
        return [i for i in range(len(self.rows)) if self.rows[i].case == case]

    def find_cases(self) -> List[str]:
        cases = [r.case for r in self.rows]
        return reduce(lambda x, y: x + [y] if y not in x else x, cases, [])

    def remove(self, item):
        del self.rows[item]
