class MissingCaptureGroupException(Exception):
    def __init__(self, pattern, message='pattern must include one capture group'):
        super(Exception).__init__(message)
        self.add_note(pattern)
