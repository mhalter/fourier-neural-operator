from argparse import ArgumentParser

from ui import SampleSelectionUI

parser = ArgumentParser(prog='sample_selection',
                        description='select grid location to sample specific cases')

parser.add_argument('-n', dest='N', type=int, required=True,
                    help='extent of each spatial dimension (only impacts visual resolution of selected sample)')
parser.add_argument('-s', dest='structure_path', type=str, required=True,
                    help='path to the .vtk files (e.g. C0001.vtk)')
parser.add_argument('-c', dest='csv_path', type=str, required=True,
                    help='path to the .csv file (will be edited if it already exists)')


if __name__ == '__main__':
    args = parser.parse_args()
    ui = SampleSelectionUI(args.N, args.structure_path, args.csv_path, 94)
    ui.start()
