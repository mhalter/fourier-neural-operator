from shutil import rmtree
from os.path import join
from argparse import ArgumentParser

import numpy as np

from utils.csv_helper import CsvFile
from utils.filesystem_helper import extract_vtk
from data import VtkSample

parser = ArgumentParser(prog='sample_selection',
                        description='select grid location to sample specific cases')

parser.add_argument('-n', dest='N', type=int, required=True,
                    help='extent of each spatial dimension')
parser.add_argument('-s', dest='source_path', type=str, required=True,
                    help='path to the case direcotries conatining .vtk files series')
parser.add_argument('-d', dest='dest_path', type=str, required=True,
                    help='path to the direcotry the dataset is saved in')
parser.add_argument('-c', dest='csv_path', type=str, required=True,
                    help='path to the .csv file describing the sample selection')
parser.add_argument('-t', dest='tmp_path', type=str, required=True,
                    help='path to the temporary directory')


if __name__ == '__main__':
    args = parser.parse_args()
    dataset = VtkSample(args.N)
    csv_file = CsvFile()
    csv_file.load(args.csv_path)
    cases = csv_file.find_cases()
    for case in cases:
        extract_vtk(join(args.source_path, case, 'cfd0/VTK.tar.gz'), join(args.tmp_path, case), True)
        rows = csv_file.find_rows(case)
        N = len(rows)
        grid, t, d, b = dataset.process(join(args.tmp_path, case, 'VTK'), [csv_file.rows[r] for r in rows])
        np.savez_compressed(join(args.dest_path, f'{case}_N{N}'), grid=grid, t=t, data=d, boundary_encoding=b)
        rmtree(join(args.tmp_path, case))
