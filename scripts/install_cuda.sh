#!/bin/bash

# Adapt the following version numbers according to your needs
cuda_version_major='11.4'
cuda_version_minor='000'
driver_version_major='470'
driver_version_minor='141.03'
cuda_version="${cuda_version_major}.${cuda_version_minor}_${driver_version_major}.${driver_version_minor}"

# Adapt the following directory locations according to your needs
cuda_install_dir="/scratch/${USER}/cuda/${cuda_version}"
TMPDIR="/scratch/${USER}/tmp"

cuda_installer="cuda_${cuda_version}_linux.run"

mkdir -p "${cuda_install_dir}" "${TMPDIR}"
if [[ ! -f "${TMPDIR}/${cuda_installer}" ]]; then
    wget "http://developer.download.nvidia.com/compute/cuda/${cuda_version_major}/Prod/local_installers/${cuda_installer}" -O "${TMPDIR}/${cuda_installer}"
fi
if [[ ! -x "${TMPDIR}/${cuda_installer}" ]]; then
    chmod 700 "${TMPDIR}/${cuda_installer}"
fi
echo 'Installing, please be patient.'
if "${TMPDIR}/${cuda_installer}" --silent --override --toolkit --installpath="${cuda_install_dir}" --toolkitpath="${cuda_install_dir}" --no-man-page --tmpdir="${TMPDIR}"; then
    echo 'Done.'
    echo
    echo "To use CUDA Toolkit ${cuda_version_major}.${cuda_version_minor}, extend your environment as follows:"
    echo
    if [[ -z ${PATH} ]]; then
        echo "export PATH=${cuda_install_dir}/bin"
    else
        echo "export PATH=${cuda_install_dir}/bin:\${PATH}"
    fi
    if [[ -z ${LD_LIBRARY_PATH} ]]; then
        echo "export LD_LIBRARY_PATH=${cuda_install_dir}/lib64"
    else
        echo "export LD_LIBRARY_PATH=${cuda_install_dir}/lib64:\${LD_LIBRARY_PATH}"
    fi
else
    cat /tmp/cuda-installer.log
fi