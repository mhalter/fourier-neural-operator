import os
import shutil
from argparse import ArgumentParser

from data import extract_vtk

parser = ArgumentParser()
parser.add_argument('-s', dest='src', type=str, required=True,
                    help='path to source directory')
parser.add_argument('-d', dest='dest', tpye=str, required=True,
                    help='path to destination directory')
parser.add_argument('-t', dest='tmp', type=str, required=True,
                    help='path to temporary directory')


if __name__ == '__main__':
    args = parser.parse_args()

    sample_path = os.path.join(args.dest, 'structure_samples')

    if not os.path.isdir(sample_path):
        os.mkdir(sample_path)

    tar_files = [(d, os.path.join(args.src, d, 'cfd0', 'VTK.tar.gz')) for d in os.listdir(args.src)]

    for f in tar_files:
        extract_vtk(f[1], os.path.join(args.tmp, f[0]), False)
        tmp_dir = os.path.join(args.tmp, f[0], 'VTK')
        path = next(iter(os.listdir(tmp_dir)))
        path_src = os.path.join(tmp_dir, path)
        path_dest = os.path.join(sample_path, f'{f[0]}.vtk')
        shutil.move(path_src, path_dest)
